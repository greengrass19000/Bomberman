###  OOP-BTL Nhóm 1

-  Thành viên :   
    - Lê Quang Khôi (TV1) 
    - Nguyễn Bá Nam Dũng (TV2)
- Hướng dẫn chạy: 
    - Chạy hàm main trong file src/uet/oop/bomberman/BombermanGame.java.
    - Trong một số môi trường nếu có lỗi thì thử chạy hàm main trong src/uet/oop/bomberman/Launcher.java

### Các chức năng hiện có:

| Tên chức năng | Người phụ trách | Ghi chú |
| ------------- | --------------- | ------- |
| Đặt bom, xử lý bom nổ | TV2 | Bấm Space hoặc Enter để đặt bomb |
| Dùng vật phẩm | TV2 | 3 vật phẩm gồm : tăng tốc, tăng số lượng và sức mạnh cho bom |
| Enemy thông minh | TV1 | Dùng thuật toán A* để chỉ dẫn di chuyển cho quái |
| Di chuyển nhân vật | TV1&2 | Di chuyển bằng WASD hoặc mũi tên. Có hỗ trợ "lướt" khi bị chặn ở góc giúp di chuyển dễ hơn |
| Sound effect | TV1&2 | |
| Đọc dữ liệu map | TV2 | Đọc map từ file .txt |
| Chia bố cục các lớp kế thừa | TV2 | |
| Hiển thị fps | TV2 | Hiển thị thông số trên tiêu đề cửa sổ |
| Tương tác giữa các thực thể | TV2 | Lửa tiêu diệt thực thể, quái vật tiêu diệt Bomberman,... |
| Tải nhiều level | TV1 | Sau màn cuối cùng hoặc khi thua hiển thị thông báo. Có thể chọn chơi lại từ đầu |
| Xử lý va chạm | TV1 | Các nhân vật không đi qua được tường. Bomberman cũng bị chặn bởi bomb mình đặt ra |

### Demo:

[Video demo các chức năng](https://drive.google.com/file/d/1p-yviecO8Ea7JBcpCYR51Uj1Jgs9Zjxi/view?usp=sharing)
