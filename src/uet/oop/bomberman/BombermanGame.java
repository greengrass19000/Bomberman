package uet.oop.bomberman;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import uet.oop.bomberman.entities.destroyable_entities.DestroyableEntity;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.sound.Sound;

public class BombermanGame extends Application {

    public static final int WIDTH = 15;
    public static final int HEIGHT = 13;

    private final long[] frameTimes = new long[100];
    private int frameTimeIndex = 0 ;
    private boolean arrayFilled = false ;

    private static Stage stage;
    private GraphicsContext gc;
    private Canvas canvas;
    private static Scene playScene;

    public static Level currentLevel;
    public static int currentLevelIndex = 0;
    public final static String[] levels = new String[] {"Level1", "Level2"};

    public static void main(String[] args) {
        System.setProperty("quantum.multithreaded", "true");
        System.setProperty("javafx.animation.fullspeed", "true");
        System.setProperty("javafx.animation.pulse", "200");
        System.setProperty("javafx.animation.framerate", "200");
        Application.launch(BombermanGame.class);
    }

    @Override
    public void start(Stage stage) {
        BombermanGame.stage = stage;

        canvas = new Canvas(Sprite.SCALED_SIZE * WIDTH, Sprite.SCALED_SIZE * HEIGHT);
        gc = canvas.getGraphicsContext2D();
        Group root = new Group();
        playScene = new Scene(root);
        root.getChildren().add(canvas);

        Input input = new Input(playScene);

        stage.setTitle("Bomberman");
        stage.setScene(playScene);

        Sound.init();
        stage.show();

        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long l) {
                render();
                update();
                //Show FPS
                long oldFrameTime = frameTimes[frameTimeIndex] ;
                frameTimes[frameTimeIndex] = l ;
                frameTimeIndex = (frameTimeIndex + 1) % frameTimes.length ;
                if (frameTimeIndex == 0) {
                    arrayFilled = true ;
                }
                if (arrayFilled) {
                    long elapsedNanos = l - oldFrameTime ;
                    long elapsedNanosPerFrame = elapsedNanos / frameTimes.length ;
                    double frameRate = 1_000_000_000.0 / elapsedNanosPerFrame ;
                    if(l % 60 == 0) stage.setTitle("Bomberman | FPS :  " + (double)Math.round (frameRate * 100) / 100);
                }
            }
        };
        timer.start();

        currentLevel = new Level(levels[0]);
        input.initializeInput();
    }

    private static Scene createWinningScene() {
        StackPane root = new StackPane();
        Text text = new Text();

        text.setText("You won!. Press any key to try again");
        text.setTextAlignment(TextAlignment.CENTER);
        text.setStyle("-fx-fill: white;");
        root.setStyle("-fx-background-color: black;");
        root.getChildren().add(text);

        Scene scene = new Scene(root, Sprite.SCALED_SIZE * WIDTH, Sprite.SCALED_SIZE * HEIGHT);
        scene.setOnKeyTyped(keyEvent -> {
            restart();
        });
        return scene;
    }

    private static Scene createLosingScene() {
        StackPane root = new StackPane();
        Text text = new Text();

        text.setText("You died. Press any key to try again");
        text.setTextAlignment(TextAlignment.CENTER);
        text.setStyle("-fx-fill: white;");
        root.setStyle("-fx-background-color: black;");
        root.getChildren().add(text);

        Scene scene = new Scene(root, Sprite.SCALED_SIZE * WIDTH, Sprite.SCALED_SIZE * HEIGHT);
        scene.setOnKeyTyped(keyEvent -> {
            restart();
        });
        return scene;
    }

    public static void add(DestroyableEntity e) {
        currentLevel.addEntitiesBuffer.add(e);
    }

    public void update() {
        currentLevel.update();
    }

    public void render() {
        currentLevel.render(gc, canvas);
    }

    public static void nextLevel() {
        Status.nextLevel();
        currentLevelIndex++;
        if (currentLevelIndex >= levels.length) {
            stage.setScene(createWinningScene());
        } else {
            currentLevel = new Level(levels[currentLevelIndex]);
        }
    }

    public static void gameOver() {
        stage.setScene(createLosingScene());
    }

    public static void restart() {
        Status.reset();
        currentLevel = new Level(levels[0]);
        currentLevelIndex = 0;
        stage.setScene(playScene);
    }
}
