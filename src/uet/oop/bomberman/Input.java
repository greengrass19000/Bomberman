package uet.oop.bomberman;

import javafx.scene.Scene;

import uet.oop.bomberman.entities.destroyable_entities.moving_entities.Bomber;

public class Input {
    private final Scene scene;
    public static boolean bombSetting;

    public Input(Scene scene) {
        this.scene = scene;
    }

    public void initializeInput() {
        scene.setOnKeyPressed(event -> {
            Bomber bomberman = BombermanGame.currentLevel.getBomberman();
            switch (event.getCode()) {
                case UP:
                case W:
                    bomberman.setUp(true); break;
                case DOWN:
                case S:
                    bomberman.setDown(true); break;
                case LEFT:
                case A:
                    bomberman.setLeft(true); break;
                case RIGHT:
                case D:
                    bomberman.setRight(true); break;
            }
        });
        scene.setOnKeyReleased(event -> {
            Bomber bomberman = BombermanGame.currentLevel.getBomberman();
            switch (event.getCode()) {
                case UP:
                case W:
                    bomberman.setUp(false); break;
                case DOWN:
                case S:
                    bomberman.setDown(false); break;
                case LEFT:
                case A:
                    bomberman.setLeft(false); break;
                case RIGHT:
                case D:
                    bomberman.setRight(false); break;
            }
        });

        scene.setOnKeyTyped(keyEvent -> {
            switch (keyEvent.getCharacter()) {
                case "\r":
                case " ":
                    bombSetting = true;
                    break;

                //Cheat keys for debugging
                case "n":
                    BombermanGame.nextLevel();
                    break;
                case "m":
                    Status.powerUpSpeed();
                    break;
                case "b":
                    Status.powerUpBombCount();
                    break;
                case "v":
                    Status.powerUpFlame();
                    break;
            }
        });
    }
}

