package uet.oop.bomberman;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import uet.oop.bomberman.entities.Entity;
import uet.oop.bomberman.entities.destroyable_entities.Bomb;
import uet.oop.bomberman.entities.destroyable_entities.Brick;
import uet.oop.bomberman.entities.destroyable_entities.DestroyableEntity;
import uet.oop.bomberman.entities.destroyable_entities.moving_entities.Bomber;
import uet.oop.bomberman.entities.destroyable_entities.moving_entities.enemies.Balloon;
import uet.oop.bomberman.entities.destroyable_entities.moving_entities.enemies.Oneal;
import uet.oop.bomberman.entities.static_entities.Ability;
import uet.oop.bomberman.entities.static_entities.Grass;
import uet.oop.bomberman.entities.static_entities.Portal;
import uet.oop.bomberman.entities.static_entities.Wall;
import uet.oop.bomberman.entities.static_entities.abilities.PowerUp_Bombs;
import uet.oop.bomberman.entities.static_entities.abilities.PowerUp_Flame;
import uet.oop.bomberman.entities.static_entities.abilities.PowerUp_Speed;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.sound.Sound;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static uet.oop.bomberman.Input.bombSetting;

public class Level {
    private final List<DestroyableEntity> entities = new ArrayList<>();
    private final List<Entity> stillObjects = new ArrayList<>();

    public int gameLevel, row, column;

    public List<DestroyableEntity> addEntitiesBuffer = new ArrayList<>();
    private Bomber bomberman;

    public Level(String stage) {
        Sound.STAGE.play();
        stillObjects.clear();
        entities.clear();
        try {
            File myObj = new File("./res/levels/" + stage + ".txt");
            Scanner myReader = new Scanner(myObj);
            gameLevel = myReader.nextInt();
            column = myReader.nextInt();
            row = myReader.nextInt();
            myReader.nextLine();
            for (int i = 0; i < column; ++i) {
                String map = myReader.nextLine();
                for (int j = 0; j < row; ++j) {
                    DestroyableEntity object = null;
                    char ch = map.charAt(j);
                    switch (ch) {
                        case ' ' :
                            stillObjects.add(new Grass(j, i, Sprite.grass.getFxImage()));
                            break;
                        case '#' :
                            stillObjects.add(new Wall(j, i, Sprite.wall.getFxImage()));
                            break;
                        case 'p' :
                            stillObjects.add(new Grass(j, i, Sprite.grass.getFxImage()));
                            bomberman = new Bomber(1, 1, Sprite.player_right.getFxImage());
                            break;
                        case 'x' :
                            stillObjects.add(new Grass(j, i, Sprite.grass.getFxImage()));
                            stillObjects.add(new Portal(j, i, Sprite.portal.getFxImage()));
                            object = new Brick(j, i, Sprite.brick.getFxImage());
                            break;
                        case '*' :
                            stillObjects.add(new Grass(j, i, Sprite.grass.getFxImage()));
                            object = new Brick(j, i, Sprite.brick.getFxImage());
                            break;
                        case 'f' :
                            stillObjects.add(new Grass(j, i, Sprite.grass.getFxImage()));
                            stillObjects.add(new PowerUp_Flame(j, i, Sprite.powerup_flames.getFxImage()));
                            object = new Brick(j, i, Sprite.brick.getFxImage());
                            break;
                        case 'b' :
                            stillObjects.add(new Grass(j, i, Sprite.grass.getFxImage()));
                            stillObjects.add(new PowerUp_Bombs(j, i, Sprite.powerup_bombs.getFxImage()));
                            object = new Brick(j, i, Sprite.brick.getFxImage());
                            break;
                        case 's' :
                            stillObjects.add(new Grass(j, i, Sprite.grass.getFxImage()));
                            stillObjects.add(new PowerUp_Speed(j, i, Sprite.powerup_speed.getFxImage()));
                            object = new Brick(j, i, Sprite.brick.getFxImage());
                            break;
                        case '1' :
                            stillObjects.add(new Grass(j, i, Sprite.grass.getFxImage()));
                            object = new Balloon(j, i, Sprite.balloom_right1.getFxImage());
                            break;
                        default :
                            stillObjects.add(new Grass(j, i, Sprite.grass.getFxImage()));
                            object = new Oneal(j, i, Sprite.oneal_right1.getFxImage());
                            break;
                    }
                    if(object != null) entities.add(object);
                }
            }
            entities.add(bomberman);
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public Bomber getBomberman() {
        return bomberman;
    }

    public List<DestroyableEntity> getDestroyableEntity(int x, int y) {
        List<DestroyableEntity> res = new ArrayList<>();
        for (DestroyableEntity e : entities) {
            if(e.getXTile() == x && e.getYTile() == y) {
                res.add(e);
            }
        }
        return res;
    }

    public List<Entity> getEntity(int x, int y) {
        List<Entity> res = new ArrayList<>();
        for (Entity e : stillObjects) {
            if (e.getXTile() == x && e.getYTile() == y) {
                res.add(e);
            }
        }
        for (DestroyableEntity e : entities) {
            if (e.getXTile() == x && e.getYTile() == y) {
                res.add(e);
            }
        }
        return res;
    }

    public List<Entity> getAllObstructibles() {
        List<Entity> res = new ArrayList<>();
        for (Entity e : entities) {
            if (e instanceof Brick || e instanceof Wall || e instanceof Bomb) {
                res.add(e);
            }
        }
        for (Entity e : stillObjects) {
            if (e instanceof Wall) {
                res.add(e);
            }
        }
        return res;
    }

    public List<Entity> getMonsterTargets() {
        List<Entity> res = new ArrayList<>();
        res.add(bomberman);
        return res;
    }

    public void update() {
        if(bombSetting) {
            boolean canPlaceBomb = Status.getMaxBomb() != Status.getBombUsing();
            List<DestroyableEntity> tmp = getDestroyableEntity((bomberman.getX() + 6) / 32, (bomberman.getY() + 8) / 32);
            for (DestroyableEntity e : tmp) {
                if (e instanceof Bomb) {
                    canPlaceBomb = false;
                    break;
                }
            }
            if(canPlaceBomb) {
                entities.add(new Bomb((bomberman.getX() + 6) / 32, (bomberman.getY() + 8) / 32, Sprite.bomb.getFxImage()));
                Status.increaseBombUsing();
            }
            bombSetting = false;
        }

        List<DestroyableEntity> thingsToBeRemove = new ArrayList<>();
        List<Entity> thingsToBeRemove2 = new ArrayList<>();

        for (DestroyableEntity e : entities) {
            if (e.isRemoved())
                thingsToBeRemove.add(e);
        }

        for (Entity e : stillObjects) {
            if(e instanceof Ability && ((Ability) e).isRemoved()) {
                thingsToBeRemove2.add(e);
            }
        }

        if (!addEntitiesBuffer.isEmpty()) {
            entities.addAll(addEntitiesBuffer);
            addEntitiesBuffer.clear();
        }

        if (!thingsToBeRemove.isEmpty()) {
            entities.removeAll(thingsToBeRemove);
        }

        if(!thingsToBeRemove2.isEmpty()) {
            stillObjects.removeAll(thingsToBeRemove2);
        }

        entities.forEach(Entity::update);
    }

    public void render(GraphicsContext gc, Canvas canvas) {
        gc.fillRect(bomberman.getX(), 0, bomberman.getX() + canvas.getWidth(), canvas.getHeight());
        stillObjects.forEach(g -> g.render(gc, bomberman.getX()));
        entities.forEach(g -> g.render(gc, bomberman.getX()));
    }
}
