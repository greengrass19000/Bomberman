package uet.oop.bomberman;

public class Status {
    private static final int MAX_FLAME_LENGTH = 4;
    private static final int MAX_BOMB_COUNT = 9;
    private static int flameLength = 1;
    private static int maxBomb = 1;
    private static int bombUsing = 0;
    private static int bomberSpeed = 5;
    private static int monsterCount = 0;

    public static void reset() {
        flameLength = 1;
        maxBomb = 1;
        bomberSpeed = 5;
        bombUsing = 0;
        monsterCount = 0;
    }

    public static void nextLevel() {
        bombUsing = 0;
        monsterCount = 0;
    }

    public static int getMaxBomb() {
        return maxBomb;
    }

    public static int getBomberSpeed() {
        return bomberSpeed;
    }

    public static int getFlameLength() {
        return flameLength;
    }

    public static int getBombUsing() {
        return bombUsing;
    }

    public static void powerUpFlame() {
        if (flameLength < MAX_FLAME_LENGTH) flameLength++;
    }

    public static void powerUpBombCount() {
        if (maxBomb < MAX_BOMB_COUNT) maxBomb++;
    }

    public static void powerUpSpeed() {
        if (bomberSpeed > 1) bomberSpeed--;
    }

    public static void increaseBombUsing() {
        bombUsing++;
    }

    public static void decreaseBombUsing() {
        bombUsing--;
    }

    public static void increaseMonster() {
        monsterCount++;
    }

    public static void decreaseMonster() {
        monsterCount--;
    }

    public static boolean noMonster() {
        return monsterCount == 0;
    }
}
