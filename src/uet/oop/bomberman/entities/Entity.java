package uet.oop.bomberman.entities;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import uet.oop.bomberman.graphics.Sprite;

public abstract class Entity {
    protected int x;
    protected int y;
    protected int width = 32;
    protected int height = 32;
    // 0 : up, 1 : right, 2: down, 3 : left

    protected Image img;

    public Entity( int xUnit, int yUnit, Image img) {
        this.x = xUnit * Sprite.SCALED_SIZE;
        this.y = yUnit * Sprite.SCALED_SIZE;
        this.img = img;
    }

    public void render(GraphicsContext gc, int xa) {
        if(xa < 32 * 7) {
            xa = 32 * 7;
        } else {
            if(xa > 32 * 23) {
                xa = 32 * 23;
            }
        }
        gc.drawImage(img, x - xa + 32 * 7, y);
    }

    public abstract void update();
    protected abstract void chooseSprite();

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getXTile() {
        return x / 32;
    }

    public int getYTile() {
        return y / 32;
    }

    public boolean collide(Entity e, int xa, int ya) {
        int ex = e.getX() + xa;
        int ey = e.getY() + ya;
        return x < (ex + e.getWidth()) && (width + x) > ex
                && y < (ey + e.getHeight()) && (height + y) > ey;
    }
}
