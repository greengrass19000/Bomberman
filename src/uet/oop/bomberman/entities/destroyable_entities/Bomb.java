package uet.oop.bomberman.entities.destroyable_entities;

import javafx.scene.image.Image;
import uet.oop.bomberman.BombermanGame;
import uet.oop.bomberman.Status;
import uet.oop.bomberman.entities.Entity;
import uet.oop.bomberman.entities.destroyable_entities.moving_entities.MovingEntity;
import uet.oop.bomberman.entities.static_entities.Wall;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.sound.Sound;

import java.util.List;

public class Bomb extends DestroyableEntity {
    private int delayTime = 240;
    private boolean exploded = false;

    public Bomb(int xUnit, int yUnit, Image img) {
        super(xUnit, yUnit, img);
    }

    @Override
    protected void Die() {
        if(exploded) return;

        int flameLength = Status.getFlameLength();
        delayTime = 0;
        Status.decreaseBombUsing();
        exploded = true;
        Sound.EXPLODE.play();

        int tmpx = getXTile();
        int tmpy = getYTile();
        int[] dx = {0, 1, 0, -1};
        int[] dy = {-1, 0, 1, 0};

        for (int dir = 0; dir < 4; ++dir) {
            for (int i = 1; i <= flameLength; ++i) {
                boolean ok = true;
                List<DestroyableEntity> tmp = BombermanGame.currentLevel.getDestroyableEntity(tmpx + dx[dir] * i, tmpy + dy[dir] * i);
                for (DestroyableEntity e : tmp) {
                    if (e instanceof Brick) {
                        ok = false;
                        e.Die();
                    }
                }
                List<Entity> tmp2 = BombermanGame.currentLevel.getEntity(tmpx + dx[dir] * i, tmpy + dy[dir] * i);
                for (Entity e : tmp2) {
                    if (e instanceof Wall) {
                        ok = false;
                        break;
                    }
                }
                if(!ok) break;
                if(i != flameLength) {
                    BombermanGame.add(new Flame(tmpx + dx[dir] * i, tmpy + dy[dir] * i, Sprite.explosion_vertical.getFxImage(), dir, false));
                } else {
                    switch (dir) {
                        case 1 :
                            BombermanGame.add(new Flame(tmpx + flameLength, tmpy, Sprite.explosion_horizontal.getFxImage(), 1, true));
                            break;
                        case 3 :
                            BombermanGame.add(new Flame(tmpx - flameLength, tmpy, Sprite.explosion_horizontal.getFxImage(), 3, true));
                            break;
                        case 0 :
                            BombermanGame.add(new Flame(tmpx, tmpy - flameLength, Sprite.explosion_vertical.getFxImage(), 0, true));
                            break;
                        default :
                            BombermanGame.add(new Flame(tmpx, tmpy + flameLength, Sprite.explosion_vertical.getFxImage(), 2, true));
                            break;
                    }
                }
            }
        }

        List<DestroyableEntity> tmp = BombermanGame.currentLevel.getDestroyableEntity(tmpx, tmpy);
        for (DestroyableEntity e : tmp) {
            if (e instanceof Brick) {
                e.Die();
            }
        }
    }

    @Override
    public void update() {
        animate();
        if(exploded) {
            List<DestroyableEntity> tmp = BombermanGame.currentLevel.getDestroyableEntity((int)x / 32, (int)y / 32);
            for (DestroyableEntity e : tmp) {
                if (e instanceof Bomb && !((Bomb) e).exploded) e.Die();
                if (e instanceof MovingEntity && ((MovingEntity) e).isAlive()) {
                    if((e.getY() + 3) / 32 == y / 32)
                        e.Die();
                }
            }
        }
        if(delayTime > -21) chooseSprite();
        if(delayTime == 0) {
            Die();
        }
        --delayTime;
    }

    @Override
    protected void chooseSprite() {
        if(!exploded) {
            img = Sprite.movingSprite(Sprite.bomb, Sprite.bomb_1, Sprite.bomb_2, animate, 40).getFxImage();
        } else {
            if(delayTime > -4) img = Sprite.bomb_exploded.getFxImage();
            else if(delayTime > -8) img = Sprite.bomb_exploded1.getFxImage();
            else if(delayTime > -12) img = Sprite.bomb_exploded2.getFxImage();
            else if(delayTime > -16) img = Sprite.bomb_exploded1.getFxImage();
            else if(delayTime > -20) img = Sprite.bomb_exploded.getFxImage();
            else removed = true;
        }
    }
}
