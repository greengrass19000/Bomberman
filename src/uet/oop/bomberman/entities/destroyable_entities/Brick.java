package uet.oop.bomberman.entities.destroyable_entities;

import javafx.scene.image.Image;
import uet.oop.bomberman.graphics.Sprite;

public class Brick extends DestroyableEntity {
    public Brick(int xUnit, int yUnit, Image img) {
        super(xUnit, yUnit, img);
    }
    private boolean died = false;

    @Override
    public void update() {
        if(died) {
            if(delayTime > -21) {
                chooseSprite();
                --delayTime;
            }
        }
    }


    @Override
    protected void Die() {
        died = true;
    }

    @Override
    protected void chooseSprite() {
        if(delayTime > -6) img = Sprite.brick_exploded.getFxImage();
        else if(delayTime > -13) img = Sprite.brick_exploded1.getFxImage();
        else if(delayTime > -20) img = Sprite.brick_exploded2.getFxImage();
        else removed = true;
    }
}
