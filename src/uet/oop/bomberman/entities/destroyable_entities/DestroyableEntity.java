package uet.oop.bomberman.entities.destroyable_entities;

import javafx.scene.image.Image;
import uet.oop.bomberman.entities.Entity;

public abstract class DestroyableEntity extends Entity {
    protected int delayTime = 0;
    protected boolean removed = false;
    protected int animate = 0;
    protected final int MAX_ANIMATE = 7500;

    public DestroyableEntity(int xUnit, int yUnit, Image img) {
        super(xUnit, yUnit, img);
    }

    protected void animate() {
        if(animate < MAX_ANIMATE) animate++; else animate = 0;
    }

    protected abstract void Die();

    protected abstract void chooseSprite();

    public boolean isRemoved() {
        return removed;
    }
}
