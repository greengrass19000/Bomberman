package uet.oop.bomberman.entities.destroyable_entities;

import javafx.scene.image.Image;
import uet.oop.bomberman.BombermanGame;
import uet.oop.bomberman.entities.destroyable_entities.moving_entities.MovingEntity;
import uet.oop.bomberman.graphics.Sprite;

import java.util.List;

public class Flame extends DestroyableEntity {

    private int direction = 0;
    private boolean isTheLast = false;
    public Flame(int xUnit, int yUnit, Image img) {
        super(xUnit, yUnit, img);
    }
    public Flame(int xUnit, int yUnit, Image img, int d, boolean itl) {
        super(xUnit, yUnit, img);
        direction = d;
        isTheLast = itl;
    }

    @Override
    protected void Die() {
    }

    @Override
    public void update() {
        if(delayTime > -21) {
            chooseSprite();
            --delayTime;
        }
        List<DestroyableEntity> tmp = BombermanGame.currentLevel.getDestroyableEntity(getXTile(), getYTile());
        for (DestroyableEntity e : tmp) {
            if (e instanceof Bomb) e.Die();
            if (e instanceof MovingEntity && ((MovingEntity) e).isAlive()) {
                if((((MovingEntity) e).getY() + 3) / 32 == y / 32)
                    e.Die();
            }
        }
    }

    @Override
    protected void chooseSprite() {
        if(delayTime > -4) {
            switch (direction) {
                case 0 :
                    img = (isTheLast ? Sprite.explosion_vertical_top_last.getFxImage() : Sprite.explosion_vertical.getFxImage());
                    break;
                case 1 :
                    img = (isTheLast ? Sprite.explosion_horizontal_right_last.getFxImage() : Sprite.explosion_horizontal.getFxImage());
                    break;
                case 3 :
                    img = (isTheLast ? Sprite.explosion_horizontal_left_last.getFxImage() : Sprite.explosion_horizontal.getFxImage());
                    break;
                default :
                    img = (isTheLast ? Sprite.explosion_vertical_down_last.getFxImage() : Sprite.explosion_vertical.getFxImage());
                    break;
            }
        }
        else if(delayTime > -8) {
            switch (direction) {
                case 0 :
                    img = (isTheLast ? Sprite.explosion_vertical_top_last1.getFxImage() : Sprite.explosion_vertical1.getFxImage());
                    break;
                case 1 :
                    img = (isTheLast ? Sprite.explosion_horizontal_right_last1.getFxImage() : Sprite.explosion_horizontal1.getFxImage());
                    break;
                case 3 :
                    img = (isTheLast ? Sprite.explosion_horizontal_left_last1.getFxImage() : Sprite.explosion_horizontal1.getFxImage());
                    break;
                default :
                    img = (isTheLast ? Sprite.explosion_vertical_down_last1.getFxImage() : Sprite.explosion_vertical1.getFxImage());
                    break;
            }
        }
        else if(delayTime > -12) {
            switch (direction) {
                case 0 :
                    img = (isTheLast ? Sprite.explosion_vertical_top_last2.getFxImage() : Sprite.explosion_vertical2.getFxImage());
                    break;
                case 1 :
                    img = (isTheLast ? Sprite.explosion_horizontal_right_last2.getFxImage() : Sprite.explosion_horizontal2.getFxImage());
                    break;
                case 3 :
                    img = (isTheLast ? Sprite.explosion_horizontal_left_last2.getFxImage() : Sprite.explosion_horizontal2.getFxImage());
                    break;
                default :
                    img = (isTheLast ? Sprite.explosion_vertical_down_last2.getFxImage() : Sprite.explosion_vertical2.getFxImage());
                    break;
            }
        }
        else if(delayTime > -16) {
            switch (direction) {
                case 0 :
                    img = (isTheLast ? Sprite.explosion_vertical_top_last1.getFxImage() : Sprite.explosion_vertical1.getFxImage());
                    break;
                case 1 :
                    img = (isTheLast ? Sprite.explosion_horizontal_right_last1.getFxImage() : Sprite.explosion_horizontal1.getFxImage());
                    break;
                case 3 :
                    img = (isTheLast ? Sprite.explosion_horizontal_left_last1.getFxImage() : Sprite.explosion_horizontal1.getFxImage());
                    break;
                default :
                    img = (isTheLast ? Sprite.explosion_vertical_down_last1.getFxImage() : Sprite.explosion_vertical1.getFxImage());
                    break;
            }
        }
        else if(delayTime > -20) {
            switch (direction) {
                case 0 :
                    img = (isTheLast ? Sprite.explosion_vertical_top_last.getFxImage() : Sprite.explosion_vertical.getFxImage());
                    break;
                case 1 :
                    img = (isTheLast ? Sprite.explosion_horizontal_right_last.getFxImage() : Sprite.explosion_horizontal.getFxImage());
                    break;
                case 3 :
                    img = (isTheLast ? Sprite.explosion_horizontal_left_last.getFxImage() : Sprite.explosion_horizontal.getFxImage());
                    break;
                default :
                    img = (isTheLast ? Sprite.explosion_vertical_down_last.getFxImage() : Sprite.explosion_vertical.getFxImage());
                    break;
            }
        }
        else removed = true;
    }


}
