package uet.oop.bomberman.entities.destroyable_entities.moving_entities;

import javafx.scene.image.Image;
import uet.oop.bomberman.BombermanGame;
import uet.oop.bomberman.Status;
import uet.oop.bomberman.entities.Entity;
import uet.oop.bomberman.entities.static_entities.Ability;
import uet.oop.bomberman.entities.static_entities.Portal;
import uet.oop.bomberman.graphics.Sprite;

import uet.oop.bomberman.sound.Sound;
import java.util.List;

public class Bomber extends MovingEntity {

    public boolean up, down, left, right;

    public Bomber(int x, int y, Image img) {
        super(x, y, img);
        width = 24;
    }

    public void setDown(boolean down) {
        this.down = down;
    }

    public void setLeft(boolean left) {
        this.left = left;
    }

    public void setRight(boolean right) {
        this.right = right;
    }

    public void setUp(boolean up) {
        this.up = up;
    }

    protected void movement() {
        int xa = 0, ya = 0;
        if (up) ya--;
        if (down) ya++;
        if (left) xa--;
        if (right) xa++;
        moving = xa != 0 || ya != 0;

        //Process sounds
        playSound(xa, ya);

        //Process movement
        int limit = 1;
        if(animate % Status.getBomberSpeed() == 0) limit = 2;
        for(int i = 0; i < limit; ++i) {
            //item collecting
            List<Entity> tmp = BombermanGame.currentLevel.getEntity((int)(x + 12) / 32, (int)(y + 16) / 32);
            for (Entity e : tmp) {
                if (e instanceof Ability) {
                    ((Ability) e).absorbed();
                }
                if (e instanceof Monster) {
                    Die();
                }
                if(Status.noMonster() && e instanceof Portal) {
                    BombermanGame.nextLevel();
                }
            }

            move(xa, ya);
        }
    }

    public boolean canMove(int xa, int ya) {
        if (!withinBounds(x + xa, y + ya)) return false;
        return !blocked(xa, ya);
    }

    @Override
    public void move(int xa, int ya) {
        if (canMove(0, ya)) {
            y += ya;
            changeDirection(0, ya);
        } else {
            for (int dx = 1; dx <= 5; dx++) {
                if (canMove(dx, ya)) {
                    x += dx;
                    y += ya;
                    changeDirection(0, ya);
                    break;
                }
                if (canMove(-dx, ya)) {
                    x += -dx;
                    y += ya;
                    changeDirection(0, ya);
                    break;
                }
            }
        }
        if (canMove(xa, 0)) {
            x += xa;
            changeDirection(xa, 0);
        } else {
            for (int dy = 1; dy <= 5; dy++) {
                if (canMove(xa, dy)) {
                    x += xa;
                    y += dy;
                    changeDirection(xa, 0);
                    break;
                }
                if (canMove(xa, -dy)) {
                    x += xa;
                    y += -dy;
                    changeDirection(xa, 0);
                    break;
                }
            }
        }
    }

    private void playSound(int xa, int ya) {
        if (xa == 0 && ya == 0) return;

        changeDirection(xa, ya);
        if(direction == 0 || direction == 2) {
            if(animate % 20 == 0)
                Sound.VERTICAL.play();
        } else {
            if (animate % 20 == 0)
                Sound.HORIZONTAL.play();
        }
    }

    protected void chooseSprite() {
        if(alive){
            switch (direction) {
                case 0:
                    img = Sprite.player_up.getFxImage();
                    if (moving) {
                        img = Sprite.movingSprite(Sprite.player_up_1, Sprite.player_up_2, animate, 20).getFxImage();
                    }
                    break;
                case 2:
                    img = Sprite.player_down.getFxImage();
                    if (moving) {
                        img = Sprite.movingSprite(Sprite.player_down_1, Sprite.player_down_2, animate, 20).getFxImage();
                    }
                    break;
                case 3:
                    img = Sprite.player_left.getFxImage();
                    if (moving) {
                        img = Sprite.movingSprite(Sprite.player_left_1, Sprite.player_left_2, animate, 20).getFxImage();
                    }
                    break;
                default:
                    img = Sprite.player_right.getFxImage();
                    if (moving) {
                        img = Sprite.movingSprite(Sprite.player_right_1, Sprite.player_right_2, animate, 20).getFxImage();
                    }
                    break;
            }
        } else {
            if(delayTime > -8) img = Sprite.player_dead1.getFxImage();
            else if(delayTime > -16) img = Sprite.player_dead2.getFxImage();
            else if(delayTime > -24) img = Sprite.player_dead3.getFxImage();
            else {
                removed = true;
                BombermanGame.gameOver();
            }
        }
    }

    @Override
    protected void Die() {
        if(!alive) return;
        Sound.DEATH.play();
        alive = false;
    }
}