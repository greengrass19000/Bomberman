package uet.oop.bomberman.entities.destroyable_entities.moving_entities;

import javafx.scene.image.Image;
import uet.oop.bomberman.BombermanGame;
import uet.oop.bomberman.Status;
import uet.oop.bomberman.entities.Entity;
import uet.oop.bomberman.utility.*;

import java.util.List;

public abstract class Monster extends MovingEntity {
    public Monster(int xUnit, int yUnit, Image img) {
        super(xUnit, yUnit, img);
        Status.increaseMonster();
    }

    protected Path path;
    protected double playerDetectionRadiusSqr = -1; // Radius of -1 means it will never detect the player
    protected int speed = 5;
    protected boolean chasing;

    @Override
    protected void movement() {
        if (!alive) return;
        if (animate % speed == 0) return;
        if (!chasing) {
            scanTargets();
        }
        if (path == null) {
            wander();
        }
        pursuePath();
    }

    protected void scanTargets() {
        List<Entity> targets = BombermanGame.currentLevel.getMonsterTargets();

        double minDist = playerDetectionRadiusSqr;
        Entity closestEntity = null;

        for (Entity e : targets) {
            int tx = e.getXTile() - getXTile();
            int ty = e.getYTile() - getYTile();
            if (tx*tx + ty*ty < minDist) {
                minDist = tx*tx + ty*ty;
                closestEntity = e;
            }
        }

        if (closestEntity != null) {
            Path chasePath = new AStarPath(getXTile(), getYTile(), closestEntity.getXTile(), closestEntity.getYTile());
            if (chasePath.isValid()) {
                chasing = true;
                path = chasePath;
            }
        }
    }

    protected void wander() {
        path = new RandomDirectionPath(getXTile(), getYTile());
    }

    protected void pursuePath() {
        int direction = path.getNextDirection(x, y);

        Displacement d = Direction.toDisplacement(direction, 1);
        if (d == null) {
            clearPath();
            return;
        }
        move(d.x, d.y);
    }

    @Override
    protected void move(int xa, int ya) {
        if (blocked(xa, ya)) {
            path = null;
            return;
        }
        changeDirection(xa, ya);
        x += xa;
        y += ya;
    }

    protected void clearPath() {
        path = null;
        chasing = false;
    }

    @Override
    public void update() {
        movement();
        animate();
        chooseSprite();
        if(!alive) {
            --delayTime;
        }
    }

    @Override
    protected void Die() {
        alive = false;
        Status.decreaseMonster();
    }
}
