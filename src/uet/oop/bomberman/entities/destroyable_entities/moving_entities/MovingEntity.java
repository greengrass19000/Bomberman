package uet.oop.bomberman.entities.destroyable_entities.moving_entities;

import javafx.scene.image.Image;
import uet.oop.bomberman.BombermanGame;
import uet.oop.bomberman.entities.Entity;
import uet.oop.bomberman.entities.destroyable_entities.DestroyableEntity;

import java.util.List;

public abstract class MovingEntity extends DestroyableEntity {

    protected int direction = 1;
    protected boolean moving = false;
    protected boolean alive = true;


    public MovingEntity(int xUnit, int yUnit, Image img) {
        super(xUnit, yUnit, img);
    }

    protected abstract void movement();

    protected abstract void move(int xa, int ya);

    protected boolean withinBounds(int xa, int ya) {
        return (xa >= 32 && ya >= 32 && xa <= (BombermanGame.currentLevel.row - 2) * 32 && ya <= (BombermanGame.currentLevel.column - 2) * 32);
    }

    public boolean isAlive() {
        return alive;
    }

    protected void changeDirection(int xa, int ya) {
        //Priortize horizonal movement
        if (xa > 0) direction = 1;
        if (xa < 0) direction = 3;
        if (ya > 0) direction = 2;
        if (ya < 0) direction = 0;
    }

    protected boolean blocked(int xa, int ya) {
        List<Entity> tmp = BombermanGame.currentLevel.getAllObstructibles();
        for (Entity e : tmp) {
            //If the entity is already inside then ignore
            if (e.collide(this, 0, 0)) continue;
            if (e.collide(this, xa, ya)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void update() {
        if(!alive) {
            --delayTime;
        } else {
            movement();
            animate();
        }
        chooseSprite();
    }
}
