package uet.oop.bomberman.entities.destroyable_entities.moving_entities.enemies;

import javafx.scene.image.Image;
import uet.oop.bomberman.entities.Entity;
import uet.oop.bomberman.entities.destroyable_entities.moving_entities.Monster;
import uet.oop.bomberman.graphics.Sprite;

public class Balloon extends Monster {
    
    public Balloon(int xUnit, int yUnit, Image img) {
        super(xUnit, yUnit, img);
    }

    @Override
    protected void chooseSprite() {
        //Enemies have only two directions which are right and left
        if(alive) {
            if (direction == 3) {
                img = Sprite.movingSprite(Sprite.balloom_left1, Sprite.balloom_left2, Sprite.balloom_left3, animate, 40).getFxImage();
            } else {
                img = Sprite.movingSprite(Sprite.balloom_right1, Sprite.balloom_right2, Sprite.balloom_right3, animate, 40).getFxImage();
            }
        } else {
            if(delayTime > -60) img = Sprite.balloom_dead.getFxImage();
            else if(delayTime > -90) img = Sprite.mob_dead1.getFxImage();
            else if(delayTime > -105) img = Sprite.mob_dead2.getFxImage();
            else if(delayTime > -120) img = Sprite.mob_dead3.getFxImage();
            else removed = true;
        }
    }
}
