package uet.oop.bomberman.entities.destroyable_entities.moving_entities.enemies;

import javafx.scene.image.Image;
import uet.oop.bomberman.entities.destroyable_entities.moving_entities.Monster;
import uet.oop.bomberman.graphics.Sprite;

public class Oneal extends Monster {
    public Oneal(int xUnit, int yUnit, Image img) {
        super(xUnit, yUnit, img);
        playerDetectionRadiusSqr = 25;
    }

    @Override
    protected void chooseSprite() {
        if(alive) {
            if (direction == 3) {
                img = Sprite.movingSprite(Sprite.oneal_left1, Sprite.oneal_left2, Sprite.oneal_left3, animate, 40).getFxImage();
            } else {
                img = Sprite.movingSprite(Sprite.oneal_right1, Sprite.oneal_right2, Sprite.oneal_right3, animate, 40).getFxImage();
            }
        } else {
            if(delayTime > -60) img = Sprite.oneal_dead.getFxImage();
            else if(delayTime > -90) img = Sprite.mob_dead1.getFxImage();
            else if(delayTime > -105) img = Sprite.mob_dead2.getFxImage();
            else if(delayTime > -120) img = Sprite.mob_dead3.getFxImage();
            else removed = true;
        }
    }
}
