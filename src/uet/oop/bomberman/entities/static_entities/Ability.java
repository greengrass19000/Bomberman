package uet.oop.bomberman.entities.static_entities;

import javafx.scene.image.Image;
import uet.oop.bomberman.entities.Entity;

public abstract class Ability extends Entity {
    public Ability(int xUnit, int yUnit, Image img) {
        super(xUnit, yUnit, img);
    }
    protected boolean removed = false;
    public abstract void absorbed();

    @Override
    public void update() {

    }

    @Override
    protected void chooseSprite() {

    }

    public boolean isRemoved() {
        return removed;
    }
}
