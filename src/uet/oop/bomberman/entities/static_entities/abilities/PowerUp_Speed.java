package uet.oop.bomberman.entities.static_entities.abilities;

import javafx.scene.image.Image;
import uet.oop.bomberman.Status;
import uet.oop.bomberman.entities.static_entities.Ability;
import uet.oop.bomberman.sound.Sound;

public class PowerUp_Speed extends Ability {
    public PowerUp_Speed(int xUnit, int yUnit, Image img) {
        super(xUnit, yUnit, img);
    }
    @Override
    public void absorbed() {
        Sound.ITEM.play();
        Status.powerUpSpeed();
        removed = true;
    }

}
