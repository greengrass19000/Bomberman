package uet.oop.bomberman.sound;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.util.Objects;

public enum Sound {
    STAGE("stage", 1, true),
    EXPLODE("explosion", 10, false),
    VERTICAL("verticalmove", 10, false),
    HORIZONTAL("horizonalmove", 10, false),
    DEATH("death", 1, false),
    ITEM("item", 5, false);

    public enum Volume {
        MUTE, PLAY
    }

    public static Volume volume = Volume.PLAY;

    private final Clip[] clips;
    private final boolean isLoop;
    private int lastPlayed = 0;

    Sound(String sound, int poolSize, boolean isLoop) {
        this.isLoop = isLoop;
        clips = new Clip[poolSize];
        try {
            for (int i=0; i<clips.length; i++) {
                AudioInputStream inputStream = AudioSystem.getAudioInputStream(
                        Objects.requireNonNull(Sound.class.getResourceAsStream("/sound/" + sound + ".wav")));
                clips[i] = AudioSystem.getClip();
                clips[i].open(inputStream);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void play() {
        lastPlayed++;
        if (lastPlayed == clips.length) {
            lastPlayed = 0;
        }
        Clip clip = clips[lastPlayed];

        if (volume != Volume.MUTE && clip != null) {
            clip.stop();
            clip.setFramePosition(0);
            if (isLoop) {
                clip.loop(Clip.LOOP_CONTINUOUSLY);
            } else {
                clip.start();
            }
        }
    }

    public static void init() {
        Sound[] ignored = values();
    }
}
