package uet.oop.bomberman.utility;

class AStarNode {
    public int x;
    public int y;
    public final double heuristic;
    public final double cost;
    public AStarNode previous;

    public AStarNode(int x, int y, int goalX, int goalY, double cost, AStarNode previous) {
        this.x = x;
        this.y = y;
        this.heuristic = Math.abs(goalX - x) + Math.abs(goalY - y);
        this.cost = cost;
        this.previous = previous;
    }

    public double finalCost() {
        return heuristic + cost;
    }

    public boolean goal(int goalX, int goalY) {
        return x == goalX && y == goalY;
    }
}
