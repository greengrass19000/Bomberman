package uet.oop.bomberman.utility;

import uet.oop.bomberman.BombermanGame;
import uet.oop.bomberman.entities.Entity;

import java.util.*;

public class AStarPath implements Path {
    private final List<AStarNode> nodes = new ArrayList<>();
    private int lastReachedNodeIndex = 0;
    private final int goalX;
    private final int goalY;
    private boolean isValid = false;

    public AStarPath(int fromX, int fromY, int toX, int toY) {
        goalX = toX;
        goalY = toY;
        Queue<AStarNode> queue = new PriorityQueue<>(Comparator.comparingDouble(AStarNode::finalCost));

        boolean[][] isVisitable = new boolean[BombermanGame.currentLevel.row][BombermanGame.currentLevel.column];
        for (boolean[] row : isVisitable) {
            Arrays.fill(row, true);
        }

        //in this particular problem keeping a set of visited nodes is cumbersome
        //so let us store it along with walls information in a 2d array
        for (Entity e : BombermanGame.currentLevel.getAllObstructibles()) {
            isVisitable[e.getXTile()][e.getYTile()] = false;
        }

        AStarNode startNode = new AStarNode(fromX, fromY, toX, toY, 0, null);
        queue.add(startNode);
        isVisitable[fromX][fromY] = false;

        while (!queue.isEmpty()) {
            AStarNode node = queue.poll();

            if (node.goal(toX, toY)) {
                storePath(node);
                isValid = true;
                return;
            }

            int x = node.x;
            int y = node.y;

            for (int direction=0; direction<=3; direction++) {
                Displacement d = Direction.toDisplacement(direction, 1);
                assert d != null;
                int nx = x + d.x;
                int ny = y + d.y;
                if (MapUtil.isValid(isVisitable, nx, ny)) {
                    queue.add(new AStarNode(nx, ny, toX, toY, node.cost + 1, node));
                    isVisitable[nx][ny] = false;
                }
            }
        }
    }

    private void storePath(AStarNode node) {
        AStarNode iterNode = node;
        while (iterNode != null) {
            nodes.add(0, iterNode);
            iterNode = iterNode.previous;
        }
    }

    @Override
    public int getNextDirection(double currentX, double currentY) {
        if (currentX == goalX * 32 && currentY == goalY * 32) {
            return -1;
        }

        double dx = nodes.get(lastReachedNodeIndex).x * 32 - currentX;
        double dy = nodes.get(lastReachedNodeIndex).y * 32 - currentY;

        if (dx == 0 && dy == 0) {
            lastReachedNodeIndex++;
            dx = nodes.get(lastReachedNodeIndex).x * 32 - currentX;
            dy = nodes.get(lastReachedNodeIndex).y * 32 - currentY;
        }

        return Direction.fromDisplacement(dx, dy, -1);
    }

    @Override
    public boolean isValid() {
        return isValid;
    }
}
