package uet.oop.bomberman.utility;

public class Direction {
    public static int reverse(int direction) {
        switch (direction) {
            case 0: //up
                return 2;
            case 1: //right
                return 3;
            case 2: //down
                return 0;
            case 3: //left
                return 1;
            default:
                return -1;
        }
    }

    public static int fromDisplacement(double dx, double dy, int fallback) {
        if (dy < 0 && dx == 0) {
            return 0;
        }
        if (dx > 0 && dy == 0) {
            return 1;
        }
        if (dy > 0 && dx == 0) {
            return 2;
        }
        if (dx < 0 && dy == 0) {
            return 3;
        }
        return fallback;
    }

    public static Displacement toDisplacement(int direction, int multiplier) {
        Displacement d = new Displacement();
        switch (direction) {
            case 0: //up
                d.y = -multiplier;
                break;
            case 1: //right
                d.x = multiplier;
                break;
            case 2: //down
                d.y = multiplier;
                break;
            case 3: //left
                d.x = -multiplier;
                break;
            default:
                return null;
        }
        return d;
    }
}
