package uet.oop.bomberman.utility;

import uet.oop.bomberman.BombermanGame;

public class MapUtil {
    public static boolean isValid(boolean[][] isVisitable, int x, int y) {
        return x < BombermanGame.currentLevel.row && x >= 0
               && y < BombermanGame.currentLevel.column && y >= 0
               && isVisitable[x][y];
    }

    public static boolean isIntersection(boolean[][] isVisitable, int x, int y) {
        int openDirection = 0;
        for (int direction=0; direction<=3; direction++) {
            Displacement d = Direction.toDisplacement(direction, 1);
            assert d != null;
            int nx = x + d.x;
            int ny = y + d.y;
            if (isValid(isVisitable, nx, ny)) {
                openDirection++;
            }
        }
        return openDirection > 2;
    }
}
