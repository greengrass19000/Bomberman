package uet.oop.bomberman.utility;

public interface Path {
    int getNextDirection(double currentX, double currentY);
    boolean isValid();
}
