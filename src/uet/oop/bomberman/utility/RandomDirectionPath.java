package uet.oop.bomberman.utility;

import uet.oop.bomberman.BombermanGame;
import uet.oop.bomberman.entities.Entity;

import java.util.*;

public class RandomDirectionPath implements Path {

    private final int goalX;
    private final int goalY;
    private final int direction;

    public RandomDirectionPath(int x, int y) {
        Random random = RandomGlobal.random;
        boolean[][] isVisitable = new boolean[BombermanGame.currentLevel.row][BombermanGame.currentLevel.column];
        for (boolean[] row : isVisitable) {
            Arrays.fill(row, true);
        }
        for (Entity e : BombermanGame.currentLevel.getAllObstructibles()) {
            isVisitable[e.getXTile()][e.getYTile()] = false;
        }

        List<Integer> movableDirection = new ArrayList<>();

        for (int direction = 0; direction <= 3; direction++) {
            Displacement d = Direction.toDisplacement(direction, 1);
            assert d != null;
            int nx = x + d.x;
            int ny = y + d.y;

            if (isVisitable[nx][ny]) {
                movableDirection.add(direction);
            }
        }

        if (movableDirection.size() == 0) {
            goalX = x;
            goalY = y;
            direction = -1;
            return;
        }

        int randomDirection = movableDirection.get(random.nextInt(movableDirection.size()));
        Displacement d = Direction.toDisplacement(randomDirection, 1);
        assert d != null;

        //ignore intersection for the first step
        if (MapUtil.isValid(isVisitable, x + d.x, y + d.y)) {
            x += d.x;
            y += d.y;
        }

        while (MapUtil.isValid(isVisitable, x + d.x, y + d.y) && !MapUtil.isIntersection(isVisitable, x, y)) {
            x += d.x;
            y += d.y;
        }

        direction = randomDirection;
        goalX = x;
        goalY = y;
    }

    @Override
    public int getNextDirection(double currentX, double currentY) {
        if (currentX == goalX * 32 && currentY == goalY * 32) {
            return -1;
        }
        return direction;
    }

    @Override
    public boolean isValid() {
        return true;
    }
}
